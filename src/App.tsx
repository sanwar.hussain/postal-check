import Footer from './Components/footers'
import PostalCard from './Components/postal-card'

import './styles/app.css'

const App = () => {
	return (
		<div className='main-container'>
			<PostalCard />
			<Footer />
		</div>
	)
}

export default App
