import { POSTAL_API_URL } from '../constants'

export async function getPincodeData(pincode: String) {
	let data: Array<any> = []
	let options = {
		method: 'POST',
		body: JSON.stringify({ pincode }),
		headers: {
			'Content-type': 'application/json',
		},
	}

	await fetch(POSTAL_API_URL, options)
		.then((response) => response.json())
		.then((receivedData) => {
			data = receivedData
		})

	return data
}
