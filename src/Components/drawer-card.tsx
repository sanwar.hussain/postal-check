import { FC } from 'react'

import '../styles/drawer-card.css'
import CardRow from './card-row'
type Props = {
	postalData: any
}

const DrawerCard: FC<Props> = ({ postalData }) => {
	function handleVisitMap(latitude: String, longitude: String) {
		const mapURL = `https://maps.google.com/?q=${latitude},${longitude}`
		window.open(mapURL, '_blank')
	}
	return (
		<div className='drawer-card-container'>
			<div className='card-content'>
				{Object.keys(postalData).map((item) => (
					<CardRow title={item} data={postalData[item]} />
				))}
				{postalData.latitude && postalData.longitude ? (
					<button
						className='btn-visit-google'
						onClick={(e) =>
							handleVisitMap(postalData.latitude, postalData.longitude)
						}
					>
						Visit Location
					</button>
				) : null}
			</div>
		</div>
	)
}

export default DrawerCard
