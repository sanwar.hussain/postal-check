import { FC } from 'react'

import '../styles/footer-element.css'

type Props = {
	image: string
	url: string
	text: string
}

const FooterElement: FC<Props> = ({ image, url, text }) => {
	return (
		<div className='footer-element-container'>
			<a
				className='element-link'
				href={url}
				target='_blank'
				rel='noopener noreferrer'
			>
				<img className='element-img' src={image} alt='' />
				<p className='element-text'>{text}</p>
			</a>
		</div>
	)
}

export default FooterElement
