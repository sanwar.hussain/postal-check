import { FC } from 'react'

import '../styles/card-row.css'

type Props = { title: string; data: string }

const CardRow: FC<Props> = ({ title, data }) => {
	return (
		<div className='card-row'>
			<p className='title'>{`${title} : `}</p>
			<p className='data'>{data}</p>
		</div>
	)
}

export default CardRow
