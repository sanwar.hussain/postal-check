import { FC, useState } from 'react'
import { getPincodeData } from '../Api/postal'

import '../styles/postal-card.css'
import PostalInfoDrawer from './postal-info-drawer'

type Props = {}

const PostalCard: FC<Props> = () => {
	const [pincode, setPincode] = useState('')
	const [pincodePlaces, setPincodePlaces] = useState<Array<any>>([])
	const [showDrawer, setShowDrawer] = useState(false)
	const [hasErrored, setHasErrored] = useState(false)

	async function handleSearch(pincode: string) {
		const pincodeData: Array<any> = await getPincodeData(pincode)
		setPincodePlaces(pincodeData)
		setShowDrawer(true)
	}

	return (
		<div className='postal-card-container'>
			<div className='img-container'>
				<img
					src={require('../Assets/images/postal.png')}
					alt='img-logo'
					className='img-logo'
				/>
			</div>
			<h2 className='header-text'>Postal Code</h2>
			<p>Enter a 6 digit Indian postal code</p>
			<div className='input-container'>
				<input
					className='pincode-input'
					type='number'
					onChange={(e) => setPincode(e.target.value)}
					onFocus={() => hasErrored && setHasErrored(false)}
				/>
				<button className='btn-search' onClick={() => handleSearch(pincode)}>
					Search
				</button>
				{hasErrored && (
					<p className='error-message'>Enter a valid 6 digit pincode</p>
				)}
				{showDrawer && (
					<PostalInfoDrawer
						setShowDrawer={setShowDrawer}
						pincodePlaces={pincodePlaces}
					/>
				)}
			</div>
		</div>
	)
}

export default PostalCard
