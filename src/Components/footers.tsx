import { FC } from 'react'
import FooterElement from './footer-element'

import '../styles/footer.css'

type Props = {}

const Footer: FC<Props> = () => {
	const footerData = [
		{
			text: 'Rapid API',
			image:
				'https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/reuntzuy6gy8ssnbktye',
			url: 'https://rapidapi.com/navii/api/pin-codes-india/',
		},
		{
			text: 'Sanwar Hussain',
			image:
				'https://brandlogos.net/wp-content/uploads/2016/06/linkedin-logo-768x768.png',
			url: 'https://www.linkedin.com/in/sanwar-hussain-23613a142/',
		},
		{
			image:
				'https://gitlab.com/assets/logo-911de323fa0def29aaf817fca33916653fc92f3ff31647ac41d2c39bbe243edb.svg',
			url: 'https://www.linkedin.com/in/sanwar-hussain-23613a142/',
			text: 'Repository',
		},
	]

	return (
		<div className='footer-container'>
			{footerData.map((footer, index) => (
				<FooterElement
					key={index}
					image={footer.image}
					url={footer.url}
					text={footer.text}
				/>
			))}
		</div>
	)
}

export default Footer
