import { FC } from 'react'
import DrawerCard from './drawer-card'

import '../styles/postal-info-drawer.css'

type Props = {
	pincodePlaces: Array<any>
	setShowDrawer: any
}

const PostalInfoDrawer: FC<Props> = ({ pincodePlaces, setShowDrawer }) => {
	function handleDrawerClose() {
		setShowDrawer(false)
	}

	return (
		<div className='postal-drawer-master-container'>
			<div className='postal-drawer-container'>
				{/* <div className='state-container'>
					<img
						className='state-img'
						src='https://images.unsplash.com/photo-1600080077823-a44592513861?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80'
						alt=''
					/>
					<h3 className='state-header'>All information shown is based on the India Posts API</h3>
				</div> */}
				{pincodePlaces.map((pincodePlace, index) => (
					<DrawerCard key={index} postalData={pincodePlace} />
				))}
			</div>
			<div className='close-drawer-container'>
				<img
					className='btn-close-drawer'
					src={require('../Assets/images/close.png')}
					alt='button-close'
					title='Close drawer'
					onClick={handleDrawerClose}
				/>
			</div>
		</div>
	)
}

export default PostalInfoDrawer
